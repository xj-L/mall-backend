CREATE TABLE if NOT EXISTS goods (
    id BIGINT AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    price BIGINT NOT NULL,
    unit VARCHAR(128) NOT NULL,
    url VARCHAR(256) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
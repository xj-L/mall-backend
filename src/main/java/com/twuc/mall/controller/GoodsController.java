package com.twuc.mall.controller;

import com.twuc.mall.contract.CreateGoodsRequest;
import com.twuc.mall.contract.CreateOrderRequest;
import com.twuc.mall.domain.Goods;
import com.twuc.mall.exception.GoodsConflictException;
import com.twuc.mall.service.GoodsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.net.URI;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/goods")
@CrossOrigin(origins = "*")
public class GoodsController {
    private GoodsService service;

    public GoodsController(GoodsService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity getGoods() {
        return ResponseEntity
                .ok(service.getGoods());
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity createGoods(@RequestBody @Valid CreateGoodsRequest goodsRequest) throws GoodsConflictException {
        final Goods goods = service.createGoods(goodsRequest);
        final URI uri = linkTo(GoodsController.class).slash(goods.getId()).toUri();
        return ResponseEntity
                .created(uri)
                .build();
    }
}

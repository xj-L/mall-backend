package com.twuc.mall.controller;

import com.twuc.mall.contract.ResponseMessage;
import com.twuc.mall.exception.GoodsConflictException;
import com.twuc.mall.exception.NotHasGoodsException;
import com.twuc.mall.exception.NotHasOrderException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({NotHasOrderException.class, NotHasGoodsException.class})
    public ResponseEntity handleNotHasOrder(NotHasOrderException e) {
        final ResponseMessage responseMessage = new ResponseMessage(e.getMessage());
        return ResponseEntity
                .status(404)
                .body(responseMessage);
    }

    @ExceptionHandler({GoodsConflictException.class})
    public ResponseEntity handleGoodsConflict(GoodsConflictException e) {
        final ResponseMessage responseMessage = new ResponseMessage(e.getMessage());
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(responseMessage);
    }
}

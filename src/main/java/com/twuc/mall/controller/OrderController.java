package com.twuc.mall.controller;

import com.twuc.mall.contract.CreateOrderRequest;
import com.twuc.mall.contract.ResponseOrder;
import com.twuc.mall.domain.Orders;
import com.twuc.mall.exception.NotHasGoodsException;
import com.twuc.mall.exception.NotHasOrderException;
import com.twuc.mall.service.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*")
public class OrderController {
    private OrderService service;

    public OrderController(OrderService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity getOrders() {
        final List<ResponseOrder> responseOrders = service.getOrders()
                .stream()
                .map(orders -> new ResponseOrder(
                        orders.getId(),
                        orders.getName(),
                        orders.getPrice(),
                        orders.getUnit(),
                        orders.getCount(),
                        orders.getGoods().getId()))
                .collect(Collectors.toList());
        return ResponseEntity
                .ok(responseOrders);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOrder(@PathVariable @NotNull Long id) throws NotHasOrderException {
        service.deleteOrder(id);
        return ResponseEntity.ok(null);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity createOrder(@RequestBody @Valid CreateOrderRequest orderRequest) throws NotHasGoodsException {
        final Orders order = service.createOrder(orderRequest);
        final URI uri = linkTo(OrderController.class).slash(order.getId()).toUri();
        return ResponseEntity
                .created(uri)
                .build();
    }
}

package com.twuc.mall.domain;

import javax.persistence.*;

@Entity(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Long price;
    private String unit;
    private Long count;
    @ManyToOne
    @JoinColumn(name = "goodsId")
    private Goods goods;

    public Orders() {
    }

    public Orders(String name, Long price, String unit, Long count, Goods goods) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.count = count;
        this.goods = goods;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public Long getCount() {
        return count;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setCount(Long count) {
        this.count += count;
    }
}

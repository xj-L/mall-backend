package com.twuc.mall.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdersRepository extends JpaRepository<Orders, Long> {
    Orders findByGoodsId(Long goodsId);
}

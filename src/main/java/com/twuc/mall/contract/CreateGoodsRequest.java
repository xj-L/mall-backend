package com.twuc.mall.contract;

import javax.validation.constraints.NotNull;

public class CreateGoodsRequest {
    @NotNull
    private String name;
    @NotNull
    private Long price;
    @NotNull
    private String unit;
    private String url;

    public CreateGoodsRequest() {
    }

    public CreateGoodsRequest(String name, Long price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public CreateGoodsRequest(String name, Long price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}

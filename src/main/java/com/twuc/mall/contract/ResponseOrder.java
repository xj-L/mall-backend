package com.twuc.mall.contract;

public class ResponseOrder {
    private Long id;
    private String name;
    private Long price;
    private String unit;
    private Long count;
    private Long goodsId;

    public ResponseOrder() {
    }

    public ResponseOrder(Long id, String name, Long price, String unit, Long count, Long goodsId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.count = count;
        this.goodsId = goodsId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public Long getCount() {
        return count;
    }

    public Long getGoodsId() {
        return goodsId;
    }
}

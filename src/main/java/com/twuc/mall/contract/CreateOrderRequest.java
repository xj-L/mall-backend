package com.twuc.mall.contract;

import javax.validation.constraints.NotNull;

public class CreateOrderRequest {
    @NotNull
    private Long count;
    @NotNull
    private Long goodsId;

    public CreateOrderRequest() {
    }

    public CreateOrderRequest(Long count, Long goodsId) {
        this.count = count;
        this.goodsId = goodsId;
    }

    public Long getCount() {
        return count;
    }

    public Long getGoodsId() {
        return goodsId;
    }
}

package com.twuc.mall.service;

import com.twuc.mall.contract.CreateGoodsRequest;
import com.twuc.mall.domain.Goods;
import com.twuc.mall.domain.GoodsRepository;
import com.twuc.mall.exception.GoodsConflictException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GoodsService {
    private GoodsRepository goodsRepository;

    public GoodsService(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    public List<Goods> getGoods() {
        return goodsRepository.findAll();
    }

    public Goods createGoods(CreateGoodsRequest goodsRequest) throws GoodsConflictException {
        final Optional<Goods> findGoods = goodsRepository.findByName(goodsRequest.getName());
        if (findGoods.isPresent()) {
            throw new GoodsConflictException("商品名称已存在，请输入新的商品名称");
        }
        final Goods goods = new Goods(goodsRequest.getName(),
                goodsRequest.getPrice(),
                goodsRequest.getUnit(),
                goodsRequest.getUrl());
        return goodsRepository.save(goods);
    }
}

package com.twuc.mall.service;

import com.twuc.mall.contract.CreateOrderRequest;
import com.twuc.mall.domain.Goods;
import com.twuc.mall.domain.GoodsRepository;
import com.twuc.mall.domain.Orders;
import com.twuc.mall.domain.OrdersRepository;
import com.twuc.mall.exception.NotHasGoodsException;
import com.twuc.mall.exception.NotHasOrderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private OrdersRepository ordersRepository;
    private GoodsRepository goodsRepository;

    public OrderService(OrdersRepository ordersRepository, GoodsRepository goodsRepository) {
        this.ordersRepository = ordersRepository;
        this.goodsRepository = goodsRepository;
    }

    public List<Orders> getOrders() {
        return ordersRepository.findAll();
    }

    public void deleteOrder(Long id) throws NotHasOrderException {
        final Orders order = ordersRepository.findById(id).orElseThrow(() ->new NotHasOrderException("order not exists"));
        ordersRepository.delete(order);
    }

    public Orders createOrder(CreateOrderRequest orderRequest) throws NotHasGoodsException {
        Orders order = ordersRepository.findByGoodsId(orderRequest.getGoodsId());
        if (order != null) {
            order.setCount(orderRequest.getCount());
            ordersRepository.save(order);
        } else {
            final Goods goods = goodsRepository.findById(orderRequest.getGoodsId()).orElseThrow(() ->new NotHasGoodsException("goods not exists"));
            final Orders o = new Orders(
                    goods.getName(),
                    goods.getPrice(),
                    goods.getUnit(),
                    orderRequest.getCount(),
                    goods
            );
            order = ordersRepository.save(o);
        }
        return order;
    }
}

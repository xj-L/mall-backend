package com.twuc.mall.repository;
import com.twuc.mall.domain.Goods;
import com.twuc.mall.domain.GoodsRepository;
import com.twuc.mall.domain.Orders;
import com.twuc.mall.domain.OrdersRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OrdersRepositoryTest {
    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    private EntityManager em;

    @Test
    void should_save_orders_when_call_save_method_by_repository() {
        final Goods goods = new Goods("可乐", 18L, "瓶", "https://img14.360buyimg.com/n1/jfs/t1/17061/37/15943/229171/5cb31269E1e8695cf/a335f7a9fb189b4c.jpg");
        goodsRepository.save(goods);
        final Orders order = new Orders("可乐", 18L, "瓶", 2L, goods);
        final Orders savedOrder = ordersRepository.save(order);
        em.flush();
        em.clear();
        assertEquals(order, savedOrder);
    }

    @Test
    void should_return_goods_when_call_get_method_by_repository() {
        final Goods goods = new Goods("可乐", 18L, "瓶", "https://img14.360buyimg.com/n1/jfs/t1/17061/37/15943/229171/5cb31269E1e8695cf/a335f7a9fb189b4c.jpg");
        goodsRepository.save(goods);
        final Orders order = new Orders("可乐", 18L, "瓶", 2L, goods);
        final Orders savedOrder = ordersRepository.save(order);
        em.flush();
        em.clear();
        final Orders findOrder = ordersRepository.getOne(savedOrder.getId());
        assertNotNull(findOrder);
        assertEquals("可乐", findOrder.getName());
    }
}

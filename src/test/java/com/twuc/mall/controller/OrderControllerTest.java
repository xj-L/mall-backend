package com.twuc.mall.controller;

import com.twuc.mall.domain.Goods;
import com.twuc.mall.domain.GoodsRepository;
import com.twuc.mall.domain.Orders;
import com.twuc.mall.domain.OrdersRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
public class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    private EntityManager em;

    @Test
    void should_return_order_list_when_call_get_all_order_api() throws Exception {
        final Goods goods = new Goods("可乐", 18L, "瓶", "https://img14.360buyimg.com/n1/jfs/t1/17061/37/15943/229171/5cb31269E1e8695cf/a335f7a9fb189b4c.jpg");
        goodsRepository.save(goods);
        final Orders order = new Orders("可乐", 18L, "瓶", 2L, goods);
        final Orders savedOrder = ordersRepository.save(order);
        em.flush();
        em.clear();
        mockMvc.perform(get("/api/orders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", Matchers.is(1)));
    }

    @Test
    void should_return_status_200_when_call_delete_api() throws Exception {
        final Goods goods = new Goods("可乐", 18L, "瓶", "https://img14.360buyimg.com/n1/jfs/t1/17061/37/15943/229171/5cb31269E1e8695cf/a335f7a9fb189b4c.jpg");
        goodsRepository.save(goods);
        final Orders order = new Orders("可乐", 18L, "瓶", 2L, goods);
        final Orders savedOrder = ordersRepository.save(order);
        em.flush();
        em.clear();
        mockMvc.perform(delete("/api/orders/1"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_status_404_when_call_delete_api_with_not_has_order() throws Exception {
        mockMvc.perform(delete("/api/orders/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_status_200_when_call_create_api_with_has_goods() throws Exception {
        final Goods goods = new Goods("可乐", 18L, "瓶", null);
        goodsRepository.save(goods);
        em.flush();
        em.clear();
        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"count\": 2, \"goodsId\": 1}"))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", "http://localhost/api/orders/1"));
    }

    @Test
    void should_return_status_200_when_call_create_api_with_not_has_goods() throws Exception {
        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"count\": 2, \"goodsId\": 1}"))
                .andExpect(status().isNotFound());
    }
}
